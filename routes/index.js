var express = require("express");
var router = express.Router();
const multer = require("multer");
const path = require("path");
const storage = multer.diskStorage({
  destination: "uploads/",
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
const upload = multer({
  storage: storage,
  fileFilter: function (req, file, cb) {
    const ext = path.extname(file.originalname);
    if (
      ext !== ".png" &&
      ext !== ".jpg" &&
      ext !== ".jpeg" &&
      ext !== ".gif" &&
      ext !== ".svg"
    ) {
      return cb(new Error("Solo se permiten imágenes"));
    }
    cb(null, true);
  },
});

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.post("/upload", upload.single("file"), function (req, res, next) {
  console.log(req.file);
  res.send("Archivo cargado con éxito");
});

module.exports = router;
